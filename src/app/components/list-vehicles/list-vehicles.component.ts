import { Component, OnInit } from '@angular/core';
import { Vehicle } from './../../models/vehicles.model';
import { VehiclesService } from './../../services/vehicles.service';

@Component({
  selector: 'app-list-vehicles',
  templateUrl: './list-vehicles.component.html',
  styleUrls: ['./list-vehicles.component.css']
})
export class ListVehiclesComponent implements OnInit {
  p: number;
  total: number;
  vehicle: Vehicle;
  vehicles: Array<Vehicle> = [];
  constructor(private vehicleService: VehiclesService) { }

  ngOnInit() {
    this.getVehicles(1);
  }

  getVehicles(page: number) {
    this.vehicleService.getVehicles(page).subscribe(
      data => {
        this.p = page;
        this.total = data.count;
        this.vehicles = data.results;
        this.scrollToTop();
      }, err => {
        throw err;
      }
    );
  }

  searchVehicle(name: string) {
    this.vehicleService.searchVehicleByName(name).subscribe(
      data => {
        this.p = 1;
        this.total = data.count;
        this.vehicles = data.results;
      }, err => {
        throw err;
      }
    );
  }

  getVehicleDetail(url: string) {
    this.vehicleService.getVehicleDetail(url).subscribe(
      data => {
        console.log(data);
        this.vehicle = data.result;
      }, err => {
        throw err;
      }
    );
  }

  scrollToTop() {
    (function smoothScroll() {
        const currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
        if (currentScroll > 0) {
            window.requestAnimationFrame(smoothScroll);
            window.scrollTo(0, currentScroll - (currentScroll / 8));
        }
    })();
  }
}
