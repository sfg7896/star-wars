import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ListVehiclesComponent } from './../../list-vehicles/list-vehicles.component';

declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Output() searchCrit = new EventEmitter<string>();
  vehicleName: string;
  constructor( private listVehicle: ListVehiclesComponent ) { }

  ngOnInit() {
    this.headerAnimation();
  }

  headerAnimation() {
    $(document).ready(() => {
      $(window).scroll(() => {
        const sticky = $('.mobile-menu');
        const scroll = $(window).scrollTop();
        if (scroll >= 40) {
          sticky.addClass('fixed');
        } else {
         sticky.removeClass('fixed');
      }
      });
  });
  }

  onSubmit() {
    this.searchCrit.emit(this.vehicleName);
  }
}
