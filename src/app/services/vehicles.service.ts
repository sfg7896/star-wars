import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VehiclesService {

  constructor(private apiService: ApiService, private http: HttpClient) { }

  getVehicles(page: number): Observable<any> {
    return this.apiService.get('/vehicles/?page=' + page);
  }

  getVehicleDetail(url: string): Observable<any> {
    return this.http.get(url);
  }

  searchVehicleByName(name: string): Observable<any> {
    return this.apiService.get('/vehicles/?search=' + name);
  }
}
